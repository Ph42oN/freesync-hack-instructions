# Freesync hack linux instructions

Freesync hack can be used to enable variable refresh rate on monitors that are not detected as vrr_capable, such as older g-sync monitors on amd GPUs, or it might even work on some monitors that don't have VRR by default at all.

To do freesync hack on linux, download EDID from some freesync monitor, and edit it to have timings that work with your monitor.

You can edit it with EDID editors, such as [AW EDID Editor](https://www.analogway.com/americas/products/software-tools/aw-edid-editor/) that runs in wine, or [wxEDID](https://sourceforge.net/projects/wxedid/).

In case of my laptop, i used [this EDID](https://github.com/linuxhw/EDID/blob/master/Digital/AOC/AOC246A/0276BD74A2BE), changed resolutions and timings to match what my display got by default, and changed minimum refresh rate to 48hz that is the lowest that works properly on my display.

It took some time to find EDID that makes it work, if it doesn't work with this EDID, maybe try something else from [this repo](https://github.com/linuxhw/EDID). EDIDs from that repo must be first converted to binary format, [here is how to do it](https://github.com/linuxhw/EDID?tab=readme-ov-file#how-to-install-edid-file).

To load that modified EDID, you need kernel built with CONFIG_DRM_LOAD_EDID_FIRMWARE. Add kernel parameter `drm.edid_firmware` that points to your custom EDID, for example `drm.edid_firmware=edid/edid.bin` will point to /lib/firmware/edid/edid.bin.

After you boot kernel with that, if everything went well, you should be able to enable variable refresh rate.